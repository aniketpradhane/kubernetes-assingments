# Assingments

This is Sample template to deploy simple Web Application on kubernets using Minikube

# Prerequisite

Minikube on windwos
Kubectl 
Docker Desktop


# Clone Repository

```
git clone https://gitlab.com/self6602009/kubernetes-assingments.git
git branch -M main
```
 
# Steps Deploy YAML File


# Install Minikube follow below URL:

https://minikube.sigs.k8s.io/docs/start/

# Build Images

docker build -t Dockerfile

docker tag <ImageName> <ImageName>:<TAG>

# Set environent for windwos minikube

minikube docker-env | Invoke-Expression

# Deploy YAML File

 kubectl.exe apply -f deployment.yaml 

# Author
- Aniket Pradhane (Blazeclan Technologies)
